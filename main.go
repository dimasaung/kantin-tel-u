// 1. Edit fitur tenant
// 2. Transaksi ini menu sendiri

package main

import "fmt"

type Tenant struct {
	ID   int
	Name string
}

var tenants []Tenant // Declaring tenants slice globally

func main() {
	var choiceMainMenu int

	for {
		mainMenu()
		fmt.Scan(&choiceMainMenu)

		switch choiceMainMenu {
		case 1:
			tenantMenu()
		case 2:
			fmt.Println("Transaction")
		case 3:
			fmt.Println("Anda berhasil keluar")
			return // Exit the program
		default:
			fmt.Println("Mohon maaf, menu tidak ada. Mohon masukkan pilihan menu yang benar")
		}
	}
}

func mainMenu() {
	fmt.Println("")
	fmt.Println("+----------------------------------------------------------+")
	fmt.Println("|       Selamat Datang di Aplikasi Kantin Tel-U            |")
	fmt.Println("+----------------------------------------------------------+")
	fmt.Println("| Main Menu :                                              |")
	fmt.Println("| 1. Tenant                                                |")
	fmt.Println("| 2. Transaksi                                             |")
	fmt.Println("| 3. Keluar                                                |")
	fmt.Println("+----------------------------------------------------------+")
	fmt.Print("Masukkan pilihan Anda: ")
}

func tenantMenu() {
	var choiceTenantMenu int
	for {
		fmt.Println("")
		fmt.Println("+----------------------------------------------------------+")
		fmt.Println("|                       Menu Tenant                        |")
		fmt.Println("+----------------------------------------------------------+")
		fmt.Println("| 1. List Tenant                                           |")
		fmt.Println("| 2. Tambah Tenant                                         |")
		fmt.Println("| 3. Ubah Tenant                                           |")
		fmt.Println("| 4. Hapus Tenant                                          |")
		fmt.Println("| 5. Kembali ke Menu Utama                                 |")
		fmt.Println("+----------------------------------------------------------+")
		fmt.Print("Masukkan pilihan Anda: ")

		fmt.Scan(&choiceTenantMenu)

		switch choiceTenantMenu {
		case 1:
			getTenantList()
		case 2:
			createTenant()
		case 3:
			editTenant()
		case 4:
			deleteTenant()
		case 5:
			mainMenu()
			return
		default:
			fmt.Println("Mohon maaf, menu tidak ada. Mohon masukkan pilihan menu yang benar")
		}
	}
}

func getTenantList() {
	if len(tenants) == 0 {
		fmt.Println("Daftar tenant kosong")
		return
	}
	fmt.Println("Daftar Tenant:")
	for _, tenant := range tenants {
		fmt.Printf("ID: %d, Name: %s\n", tenant.ID, tenant.Name)
	}
}

func createTenant() {
	var tenantName string
  
	fmt.Print("Masukkan nama untuk tenant : ")
	fmt.Scan(&tenantName)

	// Incrementing ID for new tenant
	newID := len(tenants) + 1

	// Adding new tenant to the slice
	tenants = append(tenants, Tenant{ID: newID, Name: tenantName})
	fmt.Println("Tenant berhasil ditambahkan")
}

func editTenant() {
	var id int
	fmt.Print("Masukkan ID tenant yang ingin diubah: ")
	fmt.Scan(&id)

	// Check if tenant with given ID exists
	found := false
	for i, tenant := range tenants {
		if tenant.ID == id {
			found = true
			var newName string
			fmt.Printf("Masukkan nama baru untuk tenant %d: ", id)
			fmt.Scan(&newName)
			tenants[i].Name = newName
			fmt.Println("Tenant berhasil diubah")
			break
		}
	}
	if !found {
		fmt.Println("Tenant dengan ID tersebut tidak ditemukan")
	}
}

func deleteTenant() {
	var id int
	fmt.Print("Masukkan ID tenant yang ingin dihapus: ")
	fmt.Scan(&id)

	// Check if tenant with given ID exists
	found := false
	for i, tenant := range tenants {
		if tenant.ID == id {
			found = true
			// Delete tenant from slice
			tenants = append(tenants[:i], tenants[i+1:]...)
			fmt.Println("Tenant berhasil dihapus")
			break
		}
	}
	if !found {
		fmt.Println("Tenant dengan ID tersebut tidak ditemukan")
	}
}